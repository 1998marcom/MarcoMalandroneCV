# Marco Malandrone's CV

![build status](https://gitlab.com/1998marcom/MarcoMalandroneCV/badges/master/pipeline.svg?style=flat)

My CV repository. Latest pdf downloadable
[here](https://gitlab.com/1998marcom/MarcoMalandroneCV/-/jobs/artifacts/master/raw/build/MarcoMalandroneCV.pdf?job=build).
Extended version available
[here](https://gitlab.com/1998marcom/MarcoMalandroneCV/-/jobs/artifacts/master/raw/build/MarcoMalandroneCV-extended.pdf?job=build).

Also available [the US/UK version](https://gitlab.com/1998marcom/MarcoMalandroneCV/-/jobs/artifacts/master/raw/build/MarcoMalandroneCV-usuk.pdf?job=build),
which comes without the usual (in continental Europe) personal photo,
which is replaced by a sarcastic drawing of a 6-line man. Also available
[the US/UK extended
version](https://gitlab.com/1998marcom/MarcoMalandroneCV/-/jobs/artifacts/master/raw/build/MarcoMalandroneCV-usuk-extended.pdf?job=build)

You may want to check also my [personal page](https://marco.malandrone.net).

### Sections
Major sections on the left:
 * Projects and experiences (with more in the extended version)
 * Competitions
 * Education (with notable courses in the extended version)
 * Micro education (only in the extended version)

Minor sections on the right:
 * achievements
 * strenghts
 * Languages
 * Tools and libraries (only in the extended version)
 * Hobbies (only in the extended version)

### Latex compile definitions
 * `\usukversion`: removes personal photo
 * `\evers`: extends the CV with more content, to fill a second page
