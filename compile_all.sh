#!/usr/bin/env bash

export TZ=

mkdir build

latexmk -f -g -shell-escape -interaction=nonstopmode -pdf \
-pdflatex='pdflatex -shell-escape -jobname=build/MarcoMalandroneCV "\input{main}"' \
main.tex -jobname=build/MarcoMalandroneCV

latexmk -f -g -shell-escape -interaction=nonstopmode -pdf \
-pdflatex='pdflatex -shell-escape -jobname=build/MarcoMalandroneCV-usuk "\def\usukversion{}\input{main}"' \
main.tex -jobname=build/MarcoMalandroneCV-usuk

latexmk -f -g -shell-escape -interaction=nonstopmode -pdf \
-pdflatex='pdflatex -shell-escape -jobname=build/MarcoMalandroneCV-extended "\def\evers{}\input{main}"' \
main.tex -jobname=build/MarcoMalandroneCV-extended

latexmk -f -g -shell-escape -interaction=nonstopmode -pdf \
-pdflatex='pdflatex -shell-escape -jobname=build/MarcoMalandroneCV-usuk-extended "\def\usukversion{}\def\evers{}\input{main}"' \
main.tex -jobname=build/MarcoMalandroneCV-usuk-extended
